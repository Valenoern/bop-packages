Packages related to [bopwiki](https://codeberg.org/Valenoern/bopwiki) or its companion programs.


## what am I looking at?

This is a binary package (installer) repository built on the experimental [gitrelease](https://codeberg.org/Valenoern/gitrelease) system.  
You shouldn't clone it without arguments or you'll get every single version for every operating system.

You can safely get just the latest packages by using the `https` git clone url with a "shallow" option,  
ex.: `git clone --depth 1 https://codeberg.org/Valenoern/bop-packages.git`

You can further limit packages to just one linux distro / OS (ex. "manjaro") by specifying one of the repository's branches in the command:  
`git clone --depth 1 -b manjaro git@codeberg.org:Valenoern/bop-packages.git`

(although this repo currently only contains manjaro/arch packages, so that's not very necessary right now.)


## specific instructions

To install bop on manjaro:

1. install lisp runtime if needed - `pamac install sbcl`
2. first open bop-runlisp and then bopwiki - `pamac install bop-runlisp*.zst && pamac install bopwiki*.zst`

I have not actually tested these steps so it's possible I missed something, like installing specific lisp libraries with quicklisp. If this is true I'll fix it later.


## maintainer contact

`valenoern ＠ distributary.network` - public email \[might be down currently\]  
[valenoern@floss.social](https://floss.social/@Valenoern) - mastodon microblog  
[Valenoern@twitter.com](https://twitter.com/valenoern) - not checked often. use only if the others are unworkable
